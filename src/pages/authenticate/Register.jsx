
import "./authenticate.css";
import {Button, TextField} from "@mui/material";
import {useRef} from "react";
import {useNavigate} from "react-router-dom";
import axios from "axios";

const Register = () => {
    const navigate = useNavigate();

    const username = useRef(null);
    const password = useRef(null);
    const confirm_password = useRef(null);

    const submitForm = () => {
        if(password.current.value === confirm_password.current.value) {
            let user = {
                username: username.current.value,
                password: password.current.value
            }
            axios.post("localhost:8080/api/v1/users", user)
                .then(() => {
                    navigate("/login");
                });
        }
    }

    return (
      <div className="container">
          <div className="form-wrapper">
              <img className="logo" src="src/assets/logo.svg" alt="logo"/>
              <span className="form-name form-child">Lorem ipsum</span>
              <div className="form-child">
                <TextField id="username" label="Username" variant="outlined" type="username"
                           inputRef={username} required />
              </div>
              <div className="form-child">
                <TextField id="password" label="Password" variant="outlined" type="password"
                           inputRef={password} required />
              </div>
              <div className="form-child">
                <TextField id="confirm_password" label="Confirm password" variant="outlined" type="password"
                           inputRef={confirm_password} required />
              </div>
              <div className="form-button">
                <Button id="submit_button" variant="contained" color="alt_primary" onClick={submitForm}>Register</Button>
              </div>
          </div>
      </div>
    );
}
export default Register;