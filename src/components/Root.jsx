import "./root.css";
import Sidebar from "./Sidebar.jsx";
import {Outlet} from "react-router-dom";

const Root = () => {

    return (
      <div className="root">
          <Sidebar />
          <Outlet />
      </div>
    );
}

export default Root;