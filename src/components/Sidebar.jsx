import "./sidebar.css";
import {Link} from "react-router-dom";
import {Divider} from "@mui/material";

const Sidebar = () => {

    return (
        <div className="sidebar">
            <Link className="image-link" to="/home">
                <img src="src/assets/logo.svg"  alt="home"/>
            </Link>
            <Divider />
        </div>
    );
}

export default Sidebar;