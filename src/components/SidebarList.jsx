import {Link} from "react-router-dom";

const SidebarList = (props) => {

    return (
        <div>
            {props.data.map((group) =>
                <Link to={`/group/${group.id}`}>
                    <img src={group.image} alt={`${group.name}`} />
                </Link>)}
        </div>
    );
}

export default SidebarList;