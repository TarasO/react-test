import React from "react";
import ReactDOM from "react-dom/client";
import {
    createBrowserRouter,
    RouterProvider
} from "react-router-dom";
import "./index.css";
import Register from "./pages/authenticate/Register.jsx";
import {createTheme, CssBaseline, ThemeProvider} from "@mui/material";
import Login from "./pages/authenticate/Login.jsx";
import Root from "./components/Root.jsx";

const darkTheme = createTheme({
    palette: {
        mode: "dark",
        primary: {
            main: "#d08f00"
        },
        alt_primary: {
            main: "#d05700"
        },
        alt: {
            main: "#35008a"
        },
        alt_secondary: {
            main: "#2d0071"
        },
    },
});

const router = createBrowserRouter([
    {
        path: "/register",
        element: <Register />
    },
    {
        path: "/login",
        element: <Login />
    },
    {
        path: "/",
        element: <Root />,
        children: [

        ]
    },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
      <ThemeProvider theme={darkTheme}>
          <CssBaseline />
          <RouterProvider router={router} />
      </ThemeProvider>
  </React.StrictMode>,
)
